<?php
	//stylesheet registration for ACF backend styling 
	function custom_acf_styling() {
		wp_enqueue_style( 'acf-css', get_template_directory_uri() . '/assets/scss/acf.scss', false, null );
	}

	add_action('acf/input/admin_head', 'custom_acf_styling');

	/**
	 * Allows ACF to translate field and field group settings
	 * using the __() function
	 * @link https://www.advancedcustomfields.com/resources/acf-settings/
	 */

	add_filter('acf/settings/l10n', '__return_true');

	/**
 	* Sets the text domain, used when translating field
	 * and field group settings.
	 * @link https://www.advancedcustomfields.com/resources/acf-settings/
	 */

	function atframe_acf_settings_textdomain() {
		return 'lionlab';
	}

	add_filter('acf/settings/l10n_textdomain', 'atframe_acf_settings_textdomain');

 ?>