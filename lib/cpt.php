<?php 
// Register Custom Post Type fly
function create_fly_cpt() {

	$labels = array(
		'name' => _x( 'Fly', 'Post Type General Name', 'lionlab' ),
		'singular_name' => _x( 'Fly', 'Post Type Singular Name', 'lionlab' ),
		'menu_name' => _x( 'Fly', 'Admin Menu text', 'lionlab' ),
		'name_admin_bar' => _x( 'fly', 'Tilføj Ny on Toolbar', 'lionlab' ),
		'archives' => __( 'fly Archives', 'lionlab' ),
		'attributes' => __( 'fly Attributes', 'lionlab' ),
		'parent_item_colon' => __( 'Parent fly:', 'lionlab' ),
		'all_items' => __( 'Alle fly', 'lionlab' ),
		'add_new_item' => __( 'Tilføj Nyt fly', 'lionlab' ),
		'add_new' => __( 'Tilføj Ny', 'lionlab' ),
		'new_item' => __( 'Nyt fly', 'lionlab' ),
		'edit_item' => __( 'Rediger fly', 'lionlab' ),
		'update_item' => __( 'Opdater fly', 'lionlab' ),
		'view_item' => __( 'Se fly', 'lionlab' ),
		'view_items' => __( 'Se fly', 'lionlab' ),
		'search_items' => __( 'Søg fly', 'lionlab' ),
		'not_found' => __( 'Not found', 'lionlab' ),
		'not_found_in_trash' => __( 'Not found in Trash', 'lionlab' ),
		'featured_image' => __( 'Featured Image', 'lionlab' ),
		'set_featured_image' => __( 'Set featured image', 'lionlab' ),
		'remove_featured_image' => __( 'Remove featured image', 'lionlab' ),
		'use_featured_image' => __( 'Use as featured image', 'lionlab' ),
		'insert_into_item' => __( 'Insert into fly', 'lionlab' ),
		'uploaded_to_this_item' => __( 'Uploaded to this fly', 'lionlab' ),
		'items_list' => __( 'fly list', 'lionlab' ),
		'items_list_navigation' => __( 'fly list navigation', 'lionlab' ),
		'filter_items_list' => __( 'Filter fly list', 'lionlab' ),
	);
	$rewrite = array(
		'slug' => 'foelg-vores-fly',
		'with_front' => true,
		'pages' => true,
		'feeds' => true,
	);
	$args = array(
		'label' => __( 'fly', 'lionlab' ),
		'description' => __( '', 'lionlab' ),
		'labels' => $labels,
		'menu_icon' => 'dashicons-cloud',
		'supports' => array('title', 'thumbnail'),
		'taxonomies' => array(),
		'public' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'menu_position' => 5,
		'show_in_admin_bar' => true,
		'show_in_nav_menus' => true,
		'can_export' => true,
		'has_archive' => true,
		'hierarchical' => false,
		'exclude_from_search' => false,
		'show_in_rest' => true,
		'publicly_queryable' => true,
		'capability_type' => 'post',
		'rewrite' => $rewrite,
	);

	register_post_type( 'fly', $args );

}

add_action( 'init', 'create_fly_cpt', 0 );



// Register Custom Post Type Job
function create_job_cpt() {

	$labels = array(
		'name' => _x( 'Jobs', 'Post Type General Name', 'lionlab' ),
		'singular_name' => _x( 'Job', 'Post Type Singular Name', 'lionlab' ),
		'menu_name' => _x( 'Jobs', 'Admin Menu text', 'lionlab' ),
		'name_admin_bar' => _x( 'Job', 'Add New on Toolbar', 'lionlab' ),
		'archives' => __( 'Job Archives', 'lionlab' ),
		'attributes' => __( 'Job Attributes', 'lionlab' ),
		'parent_item_colon' => __( 'Parent Job:', 'lionlab' ),
		'all_items' => __( 'Alle Jobs', 'lionlab' ),
		'add_new_item' => __( 'Tilføj Nyt Job', 'lionlab' ),
		'add_new' => __( 'Tilføj Ny', 'lionlab' ),
		'new_item' => __( 'Nyt Job', 'lionlab' ),
		'edit_item' => __( 'Rediger Job', 'lionlab' ),
		'update_item' => __( 'Opdater Job', 'lionlab' ),
		'view_item' => __( 'Se Job', 'lionlab' ),
		'view_items' => __( 'Se Jobs', 'lionlab' ),
		'search_items' => __( 'Søg Job', 'lionlab' ),
		'not_found' => __( 'Not found', 'lionlab' ),
		'not_found_in_trash' => __( 'Not found in Trash', 'lionlab' ),
		'featured_image' => __( 'Featured Image', 'lionlab' ),
		'set_featured_image' => __( 'Set featured image', 'lionlab' ),
		'remove_featured_image' => __( 'Remove featured image', 'lionlab' ),
		'use_featured_image' => __( 'Use as featured image', 'lionlab' ),
		'insert_into_item' => __( 'Insert into Job', 'lionlab' ),
		'uploaded_to_this_item' => __( 'Uploaded to this Job', 'lionlab' ),
		'items_list' => __( 'Jobs list', 'lionlab' ),
		'items_list_navigation' => __( 'Jobs list navigation', 'lionlab' ),
		'filter_items_list' => __( 'Filter Jobs list', 'lionlab' ),
	);
	$rewrite = array( 
		'slug' => 'job',
		'with_front' => false,
		'pages' => true,
		'feeds' => true,
	);
	$args = array(
		'label' => __( 'Job', 'lionlab' ),
		'description' => __( '', 'lionlab' ),
		'labels' => $labels,
		'menu_icon' => 'dashicons-businessman',
		'supports' => array('title', 'editor', 'excerpt', 'thumbnail'),
		'taxonomies' => array(),
		'public' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'menu_position' => 5,
		'show_in_admin_bar' => true,
		'show_in_nav_menus' => true,
		'can_export' => true,
		'has_archive' => true,
		'hierarchical' => false,
		'exclude_from_search' => false,
		'show_in_rest' => true,
		'publicly_queryable' => true,
		'capability_type' => 'post',
		'rewrite' => $rewrite,
	);

	register_post_type( 'job', $args );

}

add_action( 'init', 'create_job_cpt', 0 );


// Register Custom Post Type Testimonial
function create_testimonial_cpt() {

	$labels = array(
		'name' => _x( 'Testimonials', 'Post Type General Name', 'lionlab' ),
		'singular_name' => _x( 'Testimonial', 'Post Type Singular Name', 'lionlab' ),
		'menu_name' => _x( 'Testimonials', 'Admin Menu text', 'lionlab' ),
		'name_admin_bar' => _x( 'Testimonial', 'Add New on Toolbar', 'lionlab' ),
		'archives' => __( 'Testimonial Archives', 'lionlab' ),
		'attributes' => __( 'Testimonial Attributes', 'lionlab' ),
		'parent_item_colon' => __( 'Parent Testimonial:', 'lionlab' ),
		'all_items' => __( 'Alle Testimonials', 'lionlab' ),
		'add_new_item' => __( 'Tilføj Ny Testimonial', 'lionlab' ),
		'add_new' => __( 'Tilføj Ny', 'lionlab' ),
		'new_item' => __( 'Ny Testimonial', 'lionlab' ),
		'edit_item' => __( 'Rediger Testimonial', 'lionlab' ),
		'update_item' => __( 'Opdater Testimonial', 'lionlab' ),
		'view_item' => __( 'Se Testimonial', 'lionlab' ),
		'view_items' => __( 'Se Testimonials', 'lionlab' ),
		'search_items' => __( 'Søg Testimonial', 'lionlab' ),
		'not_found' => __( 'Not found', 'lionlab' ),
		'not_found_in_trash' => __( 'Not found in Trash', 'lionlab' ),
		'featured_image' => __( 'Featured Image', 'lionlab' ),
		'set_featured_image' => __( 'Set featured image', 'lionlab' ),
		'remove_featured_image' => __( 'Remove featured image', 'lionlab' ),
		'use_featured_image' => __( 'Use as featured image', 'lionlab' ),
		'insert_into_item' => __( 'Insert into Testimonial', 'lionlab' ),
		'uploaded_to_this_item' => __( 'Uploaded to this Testimonial', 'lionlab' ),
		'items_list' => __( 'Testimonials list', 'lionlab' ),
		'items_list_navigation' => __( 'Testimonials list navigation', 'lionlab' ),
		'filter_items_list' => __( 'Filter Testimonials list', 'lionlab' ),
	);
	$rewrite = array(
		'slug' => 'hvorfor-vaelge-greybird',
		'with_front' => true,
		'pages' => true,
		'feeds' => true,
	);
	$args = array(
		'label' => __( 'Testimonial', 'lionlab' ),
		'description' => __( '', 'lionlab' ),
		'labels' => $labels,
		'menu_icon' => 'dashicons-heart',
		'supports' => array('title', 'excerpt', 'thumbnail'),
		'taxonomies' => array(),
		'public' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'menu_position' => 5,
		'show_in_admin_bar' => true,
		'show_in_nav_menus' => true,
		'can_export' => true,
		'has_archive' => true,
		'hierarchical' => false,
		'exclude_from_search' => false,
		'show_in_rest' => true,
		'publicly_queryable' => true,
		'capability_type' => 'post',
		'rewrite' => $rewrite,
	);
	register_post_type( 'testimonial', $args );

}

add_action( 'init', 'create_testimonial_cpt', 0 );


// Register Custom Post Type Begivenhed
function create_begivenhed_cpt() {

	$labels = array(
		'name' => _x( 'Begivenheder', 'Post Type General Name', 'lionlab' ),
		'singular_name' => _x( 'Begivenhed', 'Post Type Singular Name', 'lionlab' ),
		'menu_name' => _x( 'Begivenheder', 'Admin Menu text', 'lionlab' ),
		'name_admin_bar' => _x( 'Begivenhed', 'Add New on Toolbar', 'lionlab' ),
		'archives' => __( 'Begivenhed Archives', 'lionlab' ),
		'attributes' => __( 'Begivenhed Attributes', 'lionlab' ),
		'parent_item_colon' => __( 'Parent Begivenhed:', 'lionlab' ),
		'all_items' => __( 'Alle Begivenheder', 'lionlab' ),
		'add_new_item' => __( 'Tilføj Ny Begivenhed', 'lionlab' ),
		'add_new' => __( 'Tilføj Ny', 'lionlab' ),
		'new_item' => __( 'Ny Begivenhed', 'lionlab' ),
		'edit_item' => __( 'Rediger Begivenhed', 'lionlab' ),
		'update_item' => __( 'Opdater Begivenhed', 'lionlab' ),
		'view_item' => __( 'Se Begivenhed', 'lionlab' ),
		'view_items' => __( 'Se Begivenheder', 'lionlab' ),
		'search_items' => __( 'Søg Begivenhed', 'lionlab' ),
		'not_found' => __( 'Not found', 'lionlab' ),
		'not_found_in_trash' => __( 'Not found in Trash', 'lionlab' ),
		'featured_image' => __( 'Featured Image', 'lionlab' ),
		'set_featured_image' => __( 'Set featured image', 'lionlab' ),
		'remove_featured_image' => __( 'Remove featured image', 'lionlab' ),
		'use_featured_image' => __( 'Use as featured image', 'lionlab' ),
		'insert_into_item' => __( 'Insert into Begivenhed', 'lionlab' ),
		'uploaded_to_this_item' => __( 'Uploaded to this Begivenhed', 'lionlab' ),
		'items_list' => __( 'Begivenheder list', 'lionlab' ),
		'items_list_navigation' => __( 'Begivenheder list navigation', 'lionlab' ),
		'filter_items_list' => __( 'Filter Begivenheder list', 'lionlab' ),
	);
	$rewrite = array(
		'slug' => 'begivenheder',
		'with_front' => true,
		'pages' => true,
		'feeds' => true,
	);
	$args = array(
		'label' => __( 'Begivenhed', 'lionlab' ),
		'description' => __( '', 'lionlab' ),
		'labels' => $labels,
		'menu_icon' => 'dashicons-calendar',
		'supports' => array('title', 'editor', 'excerpt', 'thumbnail'),
		'taxonomies' => array(),
		'public' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'menu_position' => 5,
		'show_in_admin_bar' => true,
		'show_in_nav_menus' => true,
		'can_export' => true,
		'has_archive' => true,
		'hierarchical' => false,
		'exclude_from_search' => false,
		'show_in_rest' => true,
		'publicly_queryable' => true,
		'capability_type' => 'post',
		'rewrite' => $rewrite,
	);
	register_post_type( 'begivenhed', $args );

}

add_action( 'init', 'create_begivenhed_cpt', 0 );

 ?>