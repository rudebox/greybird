<?php 
get_template_part('parts/header'); 

$api_key = get_field('api_key', 'options');

//populate posts with data from SmartBird CRM
$args = array(
      'headers' => array(
        'Content-Type' => 'application/json',
        'X-Api-Key' => $api_key
));

//use query string dk, en or se for filtering 
if (ICL_LANGUAGE_CODE === 'sv') {  
  $response = wp_safe_remote_get( 'https://smartbird.dk/crm/api/listevents?tag=se', $args);
}

elseif (ICL_LANGUAGE_CODE === 'en') {
  $response = wp_safe_remote_get( 'https://smartbird.dk/crm/api/listevents?tag=en', $args);
}

else {
  $response = wp_safe_remote_get( 'https://smartbird.dk/crm/api/listevents?tag=dk', $args);
} 
 
//convert JSON string to PHP variable
$data = json_decode($response['body']); 

foreach ($data as $event) {
  $name = $event->Name;
  $ev_id = $event->Id;
  $desc = $event->Description;
  $location_name = $event->Location->Name;
  $location_address = $event->Location->Address;
  $location_zip = $event->Location->Zip;
  $location_city = $event->Location->City;
  $start = $event->StartLocalFormatted;
  $end = $event->EndLocalFormatted;
  $info_meeting = $event->IsInfomeeting;
  $available_seats = $event->AvailableSeats;
  $full = $event->IsFull;
  $hide_date_time = $event->HideDateTimeAndLocation;
  $type = $event->Type;
  $form = $event->Form;

  //convert date string to time string
  $start_time = strtotime($event->StartLocalFormatted);
  $end_time = strtotime($event->EndLocalFormatted);

  //format time string
  $start_key = date('Y-m-d', $start_time);
  $start_end_time_formatted = date('H:i', $start_time) . ' - ' . date('H:i', $end_time);
  $start_time_hour = date('H:i', $start_time);


  //prepare
  $my_post = '';

  $title_check = (get_page_by_title( $ev_id, OBJECT, 'begivenhed')); 

  //check if post exist by title
  if (empty($title_check)) {

    $my_post = array(
        'post_type' => 'begivenhed',
        'post_title'  => $ev_id,
        'post_content' => $desc,
        'post_name' => $name,
        'post_status' => 'publish'
    );

    //insert post into DB
    $post_id = wp_insert_post($my_post);
  }

  //update existing posts  
  elseif (!empty($title_check)) {

    $my_post = array(
      'ID' =>  $title_check->ID,
      'post_type' => 'begivenhed',
      'post_title'  => $ev_id,
      'post_content' => $desc,
      'post_name' => $name,
      'post_status' => 'publish'
    );

   $post_id = wp_update_post($my_post);
  }

  //update custom fields with data from Smartbird
  update_field( 'event_name', $name, $post_id ); 
  update_field( 'event_date', $start_key, $post_id ); 
  update_field( 'event_time', $start_end_time_formatted, $post_id);
  update_field( 'event_location_name', $location_name, $post_id );
  update_field( 'event_location_address', $location_address, $post_id );
  update_field( 'event_location_zip', $location_zip, $post_id );
  update_field( 'event_location_city', $location_city, $post_id );
  update_field( 'event_id', $ev_id, $post_id );
  update_field( 'event_infomeeting', $info_meeting, $post_id );
  update_field( 'event_seats', $available_seats, $post_id );
  update_field( 'event_full', $full, $post_id );
  update_field( 'event_start_time', $event->StartLocalFormatted, $post_id );
  update_field( 'event_hide_date_time', $event->HideDateTimeAndLocation, $post_id ); 
  update_field( 'event_form', $event->Form, $post_id ); 

  //delete posts if current date has passed the event date
  if ($start_key < date('Y-m-d')) {
    wp_delete_post($post_id);
  }
}


//get current date to compare with event date
$today = date('Y-m-d');

// Custom WP query query
$args_query = array(
  'post_type' => array('begivenhed'),
  'post_status' => array('publish'),
  'posts_per_page' => -1,
  'order' => 'ASC',
  'orderby' => 'meta_value', 
  'meta_query' => array(
        array(
            'key' => 'event_date',
            'value' => $today,
            'compare' => '>='
        )
    )
); 

$query = new WP_Query( $args_query );

?>

<main>

<?php 
  get_template_part('parts/page', 'header-cpt');

  $text = get_field('event_text', 'options');
?>

  <div class="page__wrap">

    <div class="page__bg page__bg--1 parallax">
      <img src="<?php echo get_template_directory_uri(); ?>/assets/img/bg_skies_3.png" alt="skies_grey_bird" loading="lazy">
    </div>

    <div class="page__bg page__bg--2 parallax">
      <img src="<?php echo get_template_directory_uri(); ?>/assets/img/bg_skies_2.png" alt="skies_grey_bird" loading="lazy">
    </div>

    <div class="page__bg page__bg--3 parallax">
      <img src="<?php echo get_template_directory_uri(); ?>/assets/img/bg_skies.png" alt="skies_grey_bird" loading="lazy">
    </div>

    <section class="events padding--both">
      <div class="wrap hpad">

        <?php echo $text; ?> 

        <div class="row flex flex--wrap">

            <?php 
            
                if ($query->have_posts() ) :  while ($query->have_posts()): $query->the_post();  

                //get thumbnail
                $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'link-boxes' );

                //post img alt tag
                $alt = get_post_meta($thumb, '_wp_attachment_image_alt', true); 

                //ACF variables - this might be redundant as we already have the data from the API
                $name = get_field('event_name');
                $date = get_field('event_date');
                $time = get_field('event_time');
                $info_meeting = get_field('event_infomeeting');
                $location_name = get_field('event_location_name');
                $address = get_field('event_location_address');
                $zip = get_field('event_location_zip');
                $city = get_field('event_location_city');
                $seats = get_field('event_seats');
                $full = get_field('event_full');
                $close = get_field('event_close');
                $date = wp_date('j. F Y', strtotime(get_field('event_date')));
                $start_time = strtotime('-2 hours', strtotime(get_field('event_start_time')));
                $hide_date_time = get_field('event_hide_date_time');

                //set timeze to get local timezone
                date_default_timezone_set('Europe/Copenhagen');

              ?>

            <a href="<?php the_permalink(); ?>" class="events__item col-xs-6 col-md-3 anim fade-up" itemscope itemtype="http://schema.org/Event">
              
              <div class="events__wrap">

                <?php 
                    //close event 1 hour before start by comparing event date time and current time - 1 hour or if event is fully booked
                    if ($full === true || $start_time <= time()) : 
                ?>
                  <div class="events__status events__status--booked"><?php _e('Fuldt booket', 'lionlab') ?></div>
                <?php endif; ?>

                <?php if ($hide_date_time === false) : ?>
                <div class="events__details" itemprop="startDate">
                    <span class="events__details-date orange"><?php echo esc_html($date); ?></span><br>
                    <span class="events__details-time white"><?php echo esc_html($time); ?></span>
                </div>
                <?php endif; ?>
                
                <?php if (!$thumb) : ?>
                  <?php if ($info_meeting === false) : ?>
                  <img class="events__img" src="<?php echo get_template_directory_uri(); ?>/assets/img/begivenhed.jpg" alt="greybird_event" loading="lazy">
                  <?php endif; ?>
                  <?php if ($info_meeting === true) : ?>  
                  <img class="events__img" src="<?php echo get_template_directory_uri(); ?>/assets/img/infomeeting_greybird.jpg" alt="greybird_event" loading="lazy"> 
                  <?php endif; ?>      

                <?php else : ?>
                <img class="events__img" src="<?php echo esc_url($thumb[0]); ?>" alt="<?php echo esc_attr($alt); ?>" loading="lazy">   
                <?php endif; ?>   
              </div>

              <header class="flights__stats">
                <h2 class="h5 orange" itemprop="name" title="<?php echo esc_html($name); ?>">
                  <?php echo esc_html($name); ?> 
                </h2>

                <?php the_excerpt(); ?>
                
                <div class="events__location" itemprop="location" itemscope itemtype="http://schema.org/Place">
                  <img src="<?php echo get_template_directory_uri(); ?>/assets/img/event-map-icon.png" alt="map-icon"> 
                  <p itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
                    <span class="events__location-adress" itemprop="name"><?php echo esc_html($location_name); ?></span>
                    <span class="events__location-adress" itemprop="streetAddress"><?php echo esc_html($address); ?></span>
                    <span itemprop="postalCode"><?php echo esc_html($zip); ?></span>
                    <span itemprop="addressLocality"><?php echo esc_html($city); ?></span>
                  </p>

                </div>

              </header>

              <span class="btn btn--ticket"><?php _e('Læs mere', 'lionlab') ?></span>

            </a>

            <?php 
                endwhile; wp_reset_postdata(); else: 
            ?>
            
            <div class="wrap hpad">
              <p><?php _e('Der er ingen indlæg i denne kategori at vise på nuværende tidspunkt', 'lionlab') ?></p>
            </div>

            <?php endif; ?>

        </div>
      </div>
    </section>

  </div>

</main>

<?php get_template_part('parts/footer'); ?>