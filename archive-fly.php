<?php 
get_template_part('parts/header'); 

// Custom WP query query
$args_query = array(
  'post_type' => array('fly'),
  'posts_per_page' => -1,
  'order' => 'DESC',
);

$query = new WP_Query( $args_query );

?>

<main>

<?php 
  get_template_part('parts/page', 'header-cpt');

  $text = get_field('fly_text', 'options');
?>

  <div class="page__wrap">

    <div class="page__bg page__bg--1 parallax">
      <img src="<?php echo get_template_directory_uri(); ?>/assets/img/bg_skies_3.png" alt="skies_grey_bird">
    </div>

    <div class="page__bg page__bg--2 parallax">
      <img src="<?php echo get_template_directory_uri(); ?>/assets/img/bg_skies_2.png" alt="skies_grey_bird">
    </div>

    <div class="page__bg page__bg--3 parallax">
      <img src="<?php echo get_template_directory_uri(); ?>/assets/img/bg_skies.png" alt="skies_grey_bird">
    </div>

    <section class="flights padding--both">
      <div class="wrap hpad">

        <?php echo $text; ?>

        <div class="row flex flex--wrap">

            <?php 
                if ($query->have_posts() ) :  while ($query->have_posts()): $query->the_post(); 

                  //get thumbnail
                  $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'flights' );

                  //post img alt tag
                  $alt = get_post_meta($thumb, '_wp_attachment_image_alt', true);  

                  //wall of variables !! 
                  $manufacturer = get_field('manufacturer');
                  $type = get_field('type');
                  $age = get_field('age');
                  $serial = get_field('serial');
                  $mtow = get_field('mtow');
                  $height = get_field('fuselage_height');
                  $length = get_field('fuselage_length');
                  $wing_span = get_field('wing_span');
                  $vne = get_field('vne');
                  $vr = get_field('vr');
                  $to_distance = get_field('takeoff_distance');
                  $la_distance = get_field('landing_distance');
                  $rate_of_climb = get_field('rate_of_climb');
                  $range = get_field('range');
                  
                  $fr_link = get_field('fr_link');
              ?>

            <div class="flights__item anim fade-up">

              <img class="flights__img" src="<?php echo esc_url($thumb[0]); ?>" alt="<?php echo esc_attr($alt); ?>">       

              <header class="flights__stats">
                <h2 class="flights__title h2 orange" itemprop="headline" title="<?php the_title_attribute(); ?>">
                  <?php the_title(); ?>
                </h2>

                <?php if ($fr_link) : ?>
                <a target="_blank" rel="noopener" href="<?php echo esc_url($fr_link); ?>"><?php _e('View on FlightRadar24', 'lionlab') ?></a>
                <?php endif; ?>
              </header>

              <div class="flights__specs">
                <table class="flights__table">
                  <tbody>
                    <tr><td><strong><?php _e('Manufacturer', 'lionlab') ?></strong></td><td><?php echo esc_html($manufacturer); ?></td></tr>
                    <tr><td><strong><?php _e('Type', 'lionlab') ?></strong></td><td><?php echo esc_html($type); ?></td></tr>
                    <tr><td><strong><?php _e('Age', 'lionlab') ?></strong></td><td><?php echo esc_html($age); ?></td></tr>
                    <tr><td><strong><?php _e('Serial', 'lionlab') ?></strong></td><td><?php echo esc_html($serial); ?></td></tr>
                    <tr><td><strong><?php _e('MTOW', 'lionlab') ?></strong></td><td><?php echo esc_html($mtow); ?></td></tr>
                    <tr><td><strong><?php _e('Fuselage Height', 'lionlab') ?></strong></td><td><?php echo esc_html($height); ?></td></tr>
                    <tr><td><strong><?php _e('Fuselage Length', 'lionlab') ?></strong></td><td><?php echo esc_html($length); ?></td></tr>
                    <tr><td><strong><?php _e('Wing Span', 'lionlab') ?></strong></td><td><?php echo esc_html($wing_span); ?></td></tr>
                    <tr><td><strong><?php _e('VNE', 'lionlab') ?></strong></td><td><?php echo esc_html($vne); ?></td></tr>
                    <tr><td><strong><?php _e('VR', 'lionlab') ?></strong></td><td><?php echo esc_html($vr); ?></td></tr>
                    <tr><td><strong><?php _e('Takeoff Distance', 'lionlab') ?></strong></td><td><?php echo esc_html($to_distance); ?></td></tr>
                    <tr><td><strong><?php _e('Landing Distance', 'lionlab') ?></strong></td><td><?php echo esc_html($la_distance); ?></td></tr>
                    <tr><td><strong><?php _e('Rate of Climb', 'lionlab') ?></strong></td><td><?php echo esc_html($rate_of_climb); ?></td></tr>
                    <tr><td><strong><?php _e('Range', 'lionlab') ?></strong></td><td><?php echo esc_html($range); ?></td></tr>
                  </tbody>
                </table>
              </div>

            </div>

            <?php endwhile; else: ?>

            <p><?php _e('Der er ingen indlæg i denne kategori at vise på nuværende tidspunkt', 'lionlab') ?></p>

          <?php endif; ?>

        </div>
      </div>
    </section>

  </div>

  <?php get_template_part('parts/cta'); ?>

</main>

<?php get_template_part('parts/footer'); ?>