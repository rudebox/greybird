<?php

get_template_part('parts/header'); the_post(); ?>

<main>
	
	<?php get_template_part('parts/page', 'header');?>

 	<div class="page__wrap">
	  <div class="page__bg page__bg--1 parallax">
	  	<img src="<?php echo get_template_directory_uri(); ?>/assets/img/bg_skies_3.png" alt="skies_grey_bird">
	  </div>

	  <div class="page__bg page__bg--2 parallax">
	  	<img src="<?php echo get_template_directory_uri(); ?>/assets/img/bg_skies_2.png" alt="skies_grey_bird">
	  </div>

	  <div class="page__bg page__bg--3 parallax">
	  	<img src="<?php echo get_template_directory_uri(); ?>/assets/img/bg_skies.png" alt="skies_grey_bird">
	  </div>


  	 <?php get_template_part('parts/content', 'layouts'); ?>

  	</div>

	<?php get_template_part('parts/cta'); ?>

</main>

<?php get_template_part('parts/footer'); ?>
