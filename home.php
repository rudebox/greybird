<?php get_template_part('parts/header'); ?>


<?php 
  get_template_part('parts/page', 'header'); 
?>

<main>

  <div class="page__wrap">

    <div class="page__bg page__bg--1 parallax">
      <img src="<?php echo get_template_directory_uri(); ?>/assets/img/bg_skies_3.png" alt="skies_grey_bird" loading="lazy">
    </div>

    <div class="page__bg page__bg--2 parallax">
      <img src="<?php echo get_template_directory_uri(); ?>/assets/img/bg_skies_2.png" alt="skies_grey_bird" loading="lazy">
    </div>

    <div class="page__bg page__bg--3 parallax">
      <img src="<?php echo get_template_directory_uri(); ?>/assets/img/bg_skies.png" alt="skies_grey_bird" loading="lazy">
    </div>

    <section class="blog padding--both">
      <div class="wrap hpad">
        <div class="row blog__row">

          <div class="col-md-3">

            <?php get_template_part('parts/sidebar', 'categories'); ?>

            <?php get_template_part('parts/sidebar', 'newsletter'); ?>

            <?php get_template_part('parts/sidebar', 'articles'); ?>

          </div>

          <div class="col-md-8 col-md-offset-1">

              <div class="row flex flex--wrap blog__grid">

                <div class="col-sm-6"></div>

              <?php 
                //counter
                $i=0;

                if (have_posts() ) : 

                while (have_posts()): the_post(); 

                //get category name of post
                $categories = get_the_category();   

                //get thumbnail
                $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'url' );

                //post img alt tag
                $alt = get_post_meta($thumb, '_wp_attachment_image_alt', true);   

                $i++;

                //feature latest post
                $class = ($i === 1) ? 'blog__post--featured col-sm-12' : '';

                //get category
                $cats = get_the_category();

                foreach ($cats as $cat) {
                  $cat_id = $cat->term_id;
                }

              ?>

              <article class="blog__post center col-sm-6 <?php echo esc_attr($class); ?> cat--<?php echo $cat_id; ?>" itemscope itemtype="http://schema.org/BlogPosting" id="blog--<?php the_title_attribute(); ?>">

                <header>
                  <img temprop="thumbnail" class="blog__img" src="<?php echo esc_url($thumb[0]); ?>" alt="<?php echo esc_attr($alt); ?>">
                  <h2 class="blog__title h3" title="<?php the_title_attribute(); ?>" itemprop="headline">
                    <?php the_title(); ?>
                  </h2>

                  <span class="blog__meta"><strong><?php echo esc_html($categories[0]->name); ?></strong> <time datetime="<?php the_time('c'); ?>"><?php the_time('d/m/Y'); ?></time></span>

                </header>

                <div itemprop="articleBody">
                  <?php the_excerpt(); ?>
                  <a href="<?php echo the_permalink(); ?>" class="btn btn--orange"><?php _e('Læs mere', 'lionlab') ?></a>
                </div>

              </article>

              <?php endwhile; else: ?>

                <p><?php _e('Der er ingen indlæg i denne kategori at vise på nuværende tidspunkt', 'lionlab') ?></p>

              <?php endif; ?>

            </div>


          </div>

        </div>
      </div>
    </section>

  </div>

</main>

<?php get_template_part('parts/footer'); ?>