<?php get_template_part('parts/header'); ?>

<main>

  <?php get_template_part('parts/page', 'header'); ?>

  <section class="404 padding--both">
  	<div class="wrap hpad center">

    	<p><strong><?php _e('Beklager, men vi kunne ikke finde siden du søgte efter.', 'lionlab') ?></strong></p>

    	<a class="btn btn--orange" href="/"><?php _e('Gå til forsiden', 'lionlab') ?></a>

	</div>
  </section>

</main>

<?php get_template_part('parts/footer'); ?>