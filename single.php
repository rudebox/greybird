<?php get_template_part('parts/header'); 

  //get thumbnail
  $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'url' );

  //post img alt tag
  $alt = get_post_meta($thumb, '_wp_attachment_image_alt', true);   

  //get category name of post
  $categories = get_the_category();   
?>

<main>

  <div class="page__wrap">

    <div class="page__bg page__bg--1 parallax">
      <img src="<?php echo get_template_directory_uri(); ?>/assets/img/bg_skies_3.png" alt="skies_grey_bird" loading="lazy">
    </div>

    <div class="page__bg page__bg--2 parallax">
      <img src="<?php echo get_template_directory_uri(); ?>/assets/img/bg_skies_2.png" alt="skies_grey_bird" loading="lazy">
    </div>

    <div class="page__bg page__bg--3 parallax">
      <img src="<?php echo get_template_directory_uri(); ?>/assets/img/bg_skies.png" alt="skies_grey_bird" loading="lazy">
    </div>

    <section class="single padding--both">
      <div class="wrap hpad">
        <div class="row flex flex--wrap single__row">

          <div class="col-sm-3">

            <?php get_template_part('parts/sidebar-categories', 'links'); ?>

            <?php get_template_part('parts/sidebar', 'newsletter'); ?>

            <?php get_template_part('parts/sidebar', 'articles'); ?>

          </div>
        
          <?php the_post(); ?>

          <article class="single__article col-sm-8 col-sm-offset-1" itemscope itemtype="http://schema.org/BlogPosting">

            <header class="center"> 
              <img temprop="thumbnail" class="blog__img" src="<?php echo esc_url($thumb[0]); ?>" alt="<?php echo esc_attr($alt); ?>">
              <h1 class="blog__title orange h3" itemprop="headline"><?php the_title(); ?></h1>
              <span class="blog__meta">
                <strong><?php echo esc_html($categories[0]->name); ?></strong> 
                <time datetime="<?php the_time('c'); ?>"><?php the_time('d/m/Y'); ?></time> 
              </span>

            </header>

            <div class="single__content" itemprop="articleBody"> 
              <?php the_content(); ?>

              <a class="share__toggle"><?php _e('Del artiklen', 'lionlab'); ?> <i class="far fa-share-square"></i></a> 

              <div class="share">
                <a class="share__link" target="_blank" rel="noopener" href="http://www.facebook.com/sharer/sharer.php?u=<?php the_permalink(); ?>"><i class="fab fa-facebook-f"></i></a>
                <a class="share__link" target="_blank" rel="noopener" href="https://www.linkedin.com/sharing/share-offsite/?url=<?php echo the_permalink(); ?>"><i class="fab fa-linkedin-in"></i></a>
                <a class="share__link" target="_blank" rel="noopener" href="https://twitter.com/intent/tweet?url=<?php the_permalink(); ?>"><i class="fab fa-twitter"></i></a>
                <a class="share__link" href="mailto:?body=<?php the_permalink(); ?>"><i class="fas fa-envelope"></i></a>
              </div>

              <?php get_template_part('parts/author'); ?>
            </div>

          </article>

        </div>
      </div>
    </section>

  </div>

</main>

<?php get_template_part('parts/footer'); ?>