<?php

/*
 * Template Name: Contact
 */

get_template_part('parts/header'); the_post(); ?>

<?php 
	//contact fields
	$title = get_field('contact_title'); 
	$text = get_field('contact_text'); 
	$adress = get_field('address', 'options'); 
  	$phone = get_field('phone', 'options'); 
  	$mail = get_field('mail', 'options'); 
?>

<main>

	<?php get_template_part('parts/page', 'header');?>

	<div class="page__wrap">
	  <div class="page__bg page__bg--1 parallax">
	  	<img src="<?php echo get_template_directory_uri(); ?>/assets/img/bg_skies_3.png" alt="skies_grey_bird">
	  </div>

	  <div class="page__bg page__bg--2 parallax">
	  	<img src="<?php echo get_template_directory_uri(); ?>/assets/img/bg_skies_2.png" alt="skies_grey_bird">
	  </div>

	  <div class="page__bg page__bg--3 parallax">
	  	<img src="<?php echo get_template_directory_uri(); ?>/assets/img/bg_skies.png" alt="skies_grey_bird">
	  </div>
	
		<section class="contact padding--both">
			<div class="wrap hpad">
				<div class="row flex flex--wrap">

					<div class="col-sm-12 padding--bottom">
						<h2 class="contact__title"><?php echo esc_html($title); ?></h2>
						<?php echo $text; ?>
					</div>

					<div class="col-sm-12 col-md-4 contact__info">
						<h3><?php _e('GreyBird <span>Pilot Academy</span>', 'lionlab') ?></h3>
						<p><?php echo $adress; ?></p>

						<a href="tel:<?php echo get_formatted_phone($phone); ?>"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/phone.png"> <?php echo esc_html($phone); ?></a><br>
						<a href="mailto:<?php echo esc_html($mail); ?>"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/mail.png"> <?php echo esc_html($mail); ?></a>
					</div>

					<div class="col-sm-12 col-md-8 contact__form">
						<h3 class="orange"><?php _e('Kontakt', 'lionlab') ?></h3>
						<?php if (ICL_LANGUAGE_CODE === 'sv') : ?>
						<?php gravity_form( 4, $display_title = false, $display_description = false, $display_inactive = false, $field_values = null, $ajax = false, 2, $echo = true ); ?>
						<?php elseif (ICL_LANGUAGE_CODE === 'en') : ?>
						<?php gravity_form( 10, $display_title = false, $display_description = false, $display_inactive = false, $field_values = null, $ajax = false, 2, $echo = true ); ?>
						<?php else : ?>
						<?php gravity_form( 2, $display_title = false, $display_description = false, $display_inactive = false, $field_values = null, $ajax = false, 2, $echo = true ); ?>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</section>

	</div>

	<?php get_template_part('parts/latest', 'events'); ?>

	<?php get_template_part('parts/cta'); ?>

</main>

<?php get_template_part('parts/footer'); ?>
