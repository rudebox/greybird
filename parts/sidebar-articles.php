<?php 
// Custom WP query query
$args_query = array(
	'posts_per_page' => 6,
	'order' => 'DESC',
	'post_type' => 'post',
	'post_stauts' => 'any',
);

$query = new WP_Query($args_query);

if ($query->have_posts() ) :
?>

<aside class="sidebar sidebar--articles">

	<h4 class="sidebar__articles-title orange"><?php _e('Mest læste artikler', 'lionlab') ?></h4>

	<?php 
		while ($query->have_posts() ) : $query->the_post(); 

		//get thumbnail
		$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'url' );

		 //post img alt tag
        $alt = get_post_meta($thumb, '_wp_attachment_image_alt', true);  

		//get category name of post
        $categories = get_the_category();   
	?>


	<a href="<?php echo the_permalink(); ?>" class="blog__post blog__post--sidebar" itemscope itemtype="http://schema.org/BlogPosting">

	  <header>
	    <img class="blog__img" src="<?php echo esc_url($thumb[0]); ?>" loading="lazy" alt="<?php echo esc_attr($alt); ?>">
	    <span class="blog__meta blog__meta--sidebar"><strong><?php echo esc_html($categories[0]->name); ?></strong> <time datetime="<?php the_time('c'); ?>"><?php the_time('d/m/Y'); ?></time></span>
	    <h5 class="blog__title" title="<?php the_title_attribute(); ?>" itemprop="headline">
	      <?php the_title(); ?>
	    </h5>

	  </header>

	  <div itemprop="articleBody">
	    <?php echo wp_trim_words(get_the_excerpt(), 10, '...'); ?> 
	  </div>

	</a>

<?php endwhile; else: ?>

	<p><?php _e('Der er ingen indlæg i denne kategori at vise på nuværende tidspunkt', 'lionlab') ?></p>

<?php wp_reset_postdata(); ?>

</aside>

<?php endif; ?>
