<?php 
	$author = get_the_author(); 
	$author_id = get_the_author_meta('ID');
	$avatar = get_field('avatar', 'user_'. $author_id );
	$title = get_field('occupation', 'user_'. $author_id);
?>

<div class="blog__author flex">
	
	<?php if ($avatar) : ?>
		
		<img class="blog__author-avatar" src="<?php echo esc_attr($avatar['sizes']['avatar']); ?>" alt="<?php echo esc_html($author); ?>">

	<?php else : ?>

		<img class="blog__author-avatar" src="<?php echo my_gravatar_url(); ?>" alt="<?php echo esc_html($author); ?>">

	<?php endif; ?>
	
	<div class="blog_author-wrap">
		<strong class="orange"><?php _e('Skrevet af', 'lionlab') ?></strong>
		<div class="blog__author-name"><?php echo esc_html($author); ?></div>
		<div class="blog__author-title"><?php echo esc_html($title); ?></div>
		<p class="blog__author-description"><?php echo (get_the_author_meta('description') ); ?></p>
	</div>

</div>