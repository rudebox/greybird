<?php  
	$title = get_field('newsletter_title', 'options');
	$text = get_field('newsletter_text', 'options');
	$id = get_field('newsletter_id', 'options');
?>

<aside class="sidebar sidebar--newsletter">
  <h4 class="sidebar__newsletter-title"><?php echo esc_html($title); ?></h4> 
  <?php echo esc_html($text); ?>
  <?php gravity_form( $id, $display_title = false, $display_description = false, $display_inactive = false, $field_values = null, $ajax = false, 2, $echo = true ); ?>
</aside>