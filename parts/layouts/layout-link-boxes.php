<?php 
/**
* Description: Lionlab link-boxes repeater field group layout
*
* @package Lionlab
* @subpackage Lionlab
* @since Version 1.0
* @author Kaspar Rudbech
*/

//sections settings
$bg = get_sub_field('bg');
$margin = get_sub_field('margin');
$title = get_sub_field('header');
$center = get_sub_field('header_center');

//newsletter variables
$newsletter_img = get_sub_field('linkbox_newsletter_img');
$text = get_field('newsletter_text', 'options');
$id = get_field('newsletter_id', 'options');

if ($center === true) {
	$class = 'center';
}

if (have_rows('linkbox') ) :
?>

<section class="link-boxes <?php echo esc_attr($bg); ?>--bg padding--<?php echo esc_attr($margin); ?>">
	<div class="wrap hpad">
		<h2 class="link-boxes__header <?php echo esc_attr($class); ?>"><?php echo $title; ?></h2>
		<div class="row flex flex--wrap">
			<?php while (have_rows('linkbox') ) : the_row(); 
				$title = get_sub_field('title');
				$text = get_sub_field('text');
				$icon = get_sub_field('icon');
				$link = get_sub_field('link');
				$link_text = get_sub_field('link_text');
			?>
			
			<a href="<?php echo esc_url($link); ?>" class="col-xs-6 col-sm-3 link-boxes__item anim fade-up">
				<?php if ($icon) : ?>
				<img class="link-boxes__img" loading="lazy" src="<?php echo esc_url($icon['sizes']['link-boxes']); ?>" alt="<?php echo esc_attr($icon['alt']); ?>">
				<?php endif; ?>
				
				<div class="link-boxes__content--wrap">
					<div class="link-boxes__content">
						<h3 class="link-boxes__title"><?php echo esc_html($title); ?></h3>
						<?php echo $text; ?>
					</div>
					<span class="link-boxes__btn btn btn--ticket"><?php echo esc_html($link_text); ?></span>
				</div>

			</a>
			<?php endwhile; ?>

		</div>
	</div>
</section>
<?php endif; ?>