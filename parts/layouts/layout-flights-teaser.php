<?php 
/**
* Description: Lionlab flights teaser repeater field group layout
*
* @package Lionlab
* @subpackage Lionlab
* @since Version 1.0
* @author Kaspar Rudbech
*/

//sections settings
$bg = get_sub_field('bg');
$margin = get_sub_field('margin');
$title = get_sub_field('header');
$center = get_sub_field('header_center');
$intro_text = get_sub_field('flights_intro_text');

if ($center === true) {
	$class = 'center';
}

if (have_rows('flights') ) :
?>

<section class="flights-teaser <?php echo esc_attr($bg); ?>--bg padding--<?php echo esc_attr($margin); ?>">
	<div class="wrap hpad">
		<h2 class="flights-teaser__header <?php echo esc_attr($class); ?>"><?php echo $title; ?></h2>
		<div class="row flex flex--wrap">
			<div class="col-md-8 col-md-offset-2">

				<?php if ($intro_text) : ?>
					<?php echo $intro_text; ?>
				<?php endif; ?>

				<?php while (have_rows('flights') ) : the_row(); 
					$title = get_sub_field('title');
					$text = get_sub_field('text');
					$img = get_sub_field('img');
					$link = get_sub_field('link');
					$link_text = get_sub_field('link_text');
				?>

				<div class="col-sm-6 flights-teaser__item center anim fade-up">
					<?php if ($img) : ?>
					<img class="flights-teaser__img" src="<?php echo esc_url($img['url']); ?>" alt="<?php echo esc_attr($icon['alt']); ?>" loading="lazy">
					<?php endif; ?>
					
					<h3 class="flights-teaser__title orange"><?php echo esc_html($title); ?></h3>
					<p><?php echo $text; ?></p>
					<a class="flights-teaser__btn btn btn--orange" href="<?php echo esc_url($link); ?>"><?php echo esc_html($link_text); ?></a>
				</div>
				<?php endwhile; ?>

			</div>
		</div>
	</div>
</section>
<?php endif; ?>