<?php
/**
 * Description: Lionlab sliders
 *
 * @package Lionlab
 * @subpackage Lionlab
 * @since Version 1.0
 * @author Kaspar Rudbech
*/

$title = get_sub_field('slides_title');
$sub_title = get_sub_field('slides_sub_title');
$caption = get_sub_field('slides_text'); 
$bg = get_sub_field('bg');
$margin = get_sub_field('margin');
$position = get_sub_field('slider_position');
$offset = get_sub_field('offset');

if ($position === 'left') {
  $class = 'flex--reverse';
}

if ($offset === '2 to 1') {
  $slider_column_class = 'is-slider--threequarter';
  $text_column_class = 'col-sm-4';
} 

elseif ($offset === '1 to 2') {
  $slider_column_class = 'is-slider--onequarter';
  $text_column_class = 'col-sm-8';
}

elseif ($offset === 'Flexible') {
  $text_column_class = 'col-sm-6';
}

if ( have_rows('slides') ) : ?>

  <section class="slider bg--<?php echo esc_attr($bg); ?> padding--<?php echo esc_attr($margin); ?>">
    <div class="wrap hpad slider__container">
      <div class="row flex flex--wrap <?php echo esc_attr($class); ?>">
        
        <?php if (!empty($title) || !empty($caption) ) : ?>
        <div class="slider__text <?php echo esc_attr($text_column_class); ?>">
          <?php if ($title) : ?>
            <h2 class="slider__title"><?php echo $title; ?></h2>
          <?php endif; ?>

          <?php if ($sub_title) : ?>
            <h6 class="slider__title--sub orange"><?php echo esc_html($sub_title); ?></h6>
          <?php endif; ?>

          <?php if ($caption) : ?>
            <?php echo $caption; ?>
          <?php endif; ?>
        </div>
        <?php endif; ?>
        
        <?php if (!empty($title) || !empty($caption) ) : ?>
        <div class="slider__track is-slider <?php echo esc_attr($slider_column_class); ?>">
        <?php else: ?>
        <div class="slider__track is-slider is-slider--full">
        <?php endif; ?>

          <?php
          // Loop through slides
          while ( have_rows('slides') ) :
            the_row();
            $image   = get_sub_field('slides_bg');
          ?>

            <div class="slider__item flex flex--valign" style="background-image: url(<?php echo esc_url($image['url']); ?>);">
            </div>

          <?php endwhile; ?>

        </div>

      </div>

    </div>

  </section>
<?php endif; ?>