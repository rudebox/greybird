<footer class="footer" id="footer" itemscope itemtype="http://schema.org/WPFooter">
	<div class="wrap hpad">
		<div class="row flex flex--wrap">
			<?php 
				if (have_rows('footer_columns', 'options') ) : while (have_rows('footer_columns', 'options') ) :
					the_row();
				$title = get_sub_field('column_title');
				$text = get_sub_field('column_text');
			 ?>

			 <div class="footer__item">
			 	<?php if ($title) : ?>
			 	<h4 class="footer__title"><?php echo esc_html($title); ?></h4>
			 	<?php endif; ?>
				<?php echo $text; ?>
			 </div>

			<?php endwhile; endif; ?>

			<?php  
				$title = get_field('newsletter_title', 'options');
				$text = get_field('newsletter_text', 'options');
				$id = get_field('newsletter_id', 'options');
			?>
			
			<div class="footer__item">
				<h4 class="footer__title"><?php echo esc_html($title); ?></h4>
				  <?php echo esc_html($text); ?>
  					<?php gravity_form( $id, $display_title = false, $display_description = false, $display_inactive = false, $field_values = null, $ajax = false, 2, $echo = true ); ?>
			</div>
			
			<div class="footer__map col-sm-6">
				<?php get_template_part('parts/google', 'maps'); ?>
			</div>

		</div>
	</div>
</footer>

<div id="fb-root"></div>
<?php 
	if (ICL_LANGUAGE_CODE === 'da') { 
		$locale = 'da_DK';
		$page_id = '235117446634543';
		$logged_in_greeting = 'Hej! Har du brug for hjælp? Jeg sidder klar til at besvare dine spørgsmål. :-)';
		$logged_out_greeting = 'Hej! Hvordan kan vi hjælpe dig?';
	} 

	elseif (ICL_LANGUAGE_CODE === 'en') { 
		$locale = 'en_US';
		$page_id = '235117446634543';
		$logged_in_greeting = "Hi! Do you need help? I'm ready to answer your questions. :-)";
		$logged_out_greeting = 'Hi! How can we help you?';
	} 

	elseif (ICL_LANGUAGE_CODE === 'sv') {
		$locale = 'sv_SE';
		$page_id = '1512212475478265';
		$logged_in_greeting = 'Hej! Behöver du hjälp? Jag är redo att svara på dina frågor. :-)';
		$logged_out_greeting = 'Hej! Hur kan vi hjälpa dig?';
	}
?>

<script>
(function(d, s, id) {
var js, fjs = d.getElementsByTagName(s)[0]; 
js = d.createElement(s); 
js.id = id;
js.src = 'https://connect.facebook.net/<?php echo esc_html($locale); ?>/sdk/xfbml.customerchat.js#xfbml=1&version=v9.0&autoLogAppEvents=1';
fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<div class="fb-customerchat"
attribution="wordpress"
greeting_dialog_delay="300"
greeting_dialog_display="fade"
theme_color="#ed7f08"
page_id="<?php echo esc_html($page_id); ?>"
logged_in_greeting="<?php echo esc_html($logged_in_greeting); ?>"
logged_out_greeting="<?php echo esc_html($logged_out_greeting); ?>"
>
</div>

<?php wp_footer(); ?>

</body>
</html>
