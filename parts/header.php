<!doctype html>

<html <?php language_attributes(); ?> class="has-js">

<head>
  <meta charset="utf-8">
  <title><?php wp_title( '|', true, 'right' ); ?></title>

  <script id="Cookiebot" src="https://consent.cookiebot.com/uc.js" data-cbid="1a875593-1c59-4a40-b9b0-3cb72945d6e1" data-blockingmode="auto" type="text/javascript"></script>

  <!-- Google Tag Manager -->
  <script data-cookieconsent="ignore">(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
  new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
  j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
  'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
  })(window,document,'script','dataLayer','GTM-K4L6LKR');</script>

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?> itemscope itemtype="https://schema.org/WebPage">

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-K4L6LKR"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>

<?php if (is_single () ) : ?>
<header class="header" id="header" itemscope itemtype="http://schema.org/WPHeader">
<?php else : ?>
<header class="header header--transparent" id="header" itemscope itemtype="http://schema.org/WPHeader">
<?php endif; ?>

  <div class="header__container wrap hpad flex flex--center flex--justify">

    <a class="header__logo" href="<?php bloginfo('url'); ?>">
      <img src="<?php echo get_template_directory_uri(); ?>/assets/img/logo.png" alt="<?php bloginfo('name'); ?>">
    </a>

    <div class="nav-toggle"> 
      <span class="nav-toggle__label"><?php _e('Menu', 'lionlab') ?></span> 
      <span class="nav-toggle__icon"></span>
    </div>

    <nav class="nav" role="navigation" itemscope itemtype="http://schema.org/SiteNavigationElement">
      <div class="nav--mobile">
        <div class="nav--mobile--bg"></div>
        <?php main_nav(); ?>
        <?php social_nav(); ?>
        <?php lang_nav(); ?> 
      </div>
    </nav>

  </div>
</header>