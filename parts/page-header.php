<?php 
	if ( is_tax() ) {
		$term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
		$title = $term->name;
	} elseif ( is_category() ) {
		$title = single_cat_title( '', false );
	} elseif ( is_archive() ) {
		$title = post_type_archive_title( '', false );
	} elseif ( is_404() ) {
		$title = __('Siden kunne ikke findes', 'lionlab');
	} elseif ( is_search() ) {
		$title = __('Søgeresultat', 'lionlab');
	} else {
		$id = (is_home()) ? get_option('page_for_posts') : $post->ID;
		$title = get_proper_title($id);
	}

	//page img
	$page_img = get_field('page_img') ? : $page_img = get_field('page_img', 'options');

	//cta
	$text = get_field('header_cta_text', 'options');
	$link = get_field('header_cta_link', 'options');

?>

<?php if (is_single () ) : ?>
<section class="page__hero" style="background-image: url(<?php echo esc_url($page_img['url']); ?>);">
<?php else : ?>
<section class="page__hero page__hero--transparent" style="background-image: url(<?php echo esc_url($page_img['url']); ?>);">
<?php endif; ?>
	<div class="page__container wrap hpad">
		
		<?php if (!is_post_type_archive() ) : ?>
		<h1 class="page__title"><?php echo $title; ?></h1>
		<?php endif; ?>

		<a class="page__cta" href="<?php echo esc_url($link); ?>">
			<div class="page__cta-wrap">
				<img class="page__cta-img" src="<?php echo get_template_directory_uri(); ?>/assets/img/flight-icon.png" alt="icon">
				<h3 class="page__cta-title"><?php echo $text; ?></h3>
			</div>
		</a>

	</div>
</section>