<?php 

	if ( is_tax() ) {
		$term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
		$title = $term->name;
	} elseif ( is_category() ) {
		$title = single_cat_title( '', false );
	} elseif ( is_archive() ) {
		$title = post_type_archive_title( '', false );
	} elseif ( is_404() ) {
		$title = __('Siden kunne ikke findes', 'lionlab');
	} elseif ( is_search() ) {
		$title = __('Søgeresultat', 'lionlab');
	} else {
		$id = (is_home()) ? get_option('page_for_posts') : $post->ID;
		$title = get_proper_title($id);
	}

	//this code seems pretty redundant 

	//cpt archive titles
	$page_title_testimonial = get_field('page_title_testimonial', 'options');
	$page_title_fly = get_field('page_title_fly', 'options');
	$page_title_job = get_field('page_title_job', 'options');
	$page_title_event = get_field('page_title_event', 'options');

	//cta
	$text = get_field('header_cta_text', 'options');
	$link = get_field('header_cta_link', 'options');

	//cpt page img
	$page_img_testimonial = get_field('page_img_testimonial', 'options'); 
	$page_img_job = get_field('page_img_job', 'options');
	$page_img_fly = get_field('page_img_fly', 'options');
	$page_img_event = get_field('page_img_event', 'options');

	//fallback
	$page_img = get_field('page_img', 'options');
 ?>

<?php if (is_post_type_archive('testimonial') && !empty ($page_img_testimonial) ) : ?>
<section class="page__hero page__hero--transparent" style="background-image: url(<?php echo esc_url($page_img_testimonial['url']); ?>);">

<?php elseif (is_post_type_archive('fly') && !empty ($page_img_fly) ) : ?>
<section class="page__hero page__hero--transparent" style="background-image: url(<?php echo esc_url($page_img_fly['url']); ?>);">

<?php elseif (is_post_type_archive('job') && !empty ($page_img_job) ) : ?>
<section class="page__hero page__hero--transparent" style="background-image: url(<?php echo esc_url($page_img_job['url']); ?>);">

<?php elseif (is_post_type_archive('begivenhed') && !empty ($page_img_event) ) : ?>
<section class="page__hero page__hero--transparent" style="background-image: url(<?php echo esc_url($page_img_event['url']); ?>);">

<?php else : ?>
<section class="page__hero page__hero--transparent" style="background-image: url(<?php echo esc_url($page_img['url']); ?>);">
<?php endif; ?>

	<div class="page__container wrap hpad">

		
		<?php if (is_post_type_archive('testimonial') && !empty ($page_title_testimonial) ) : ?>
		<h1 class="page__title"><?php echo $page_title_testimonial; ?></h1>
		<?php elseif (is_post_type_archive('testimonial') && empty($page_title_testimonial) ) : ?>
		<h1 class="page__title"><?php echo $title; ?></h1>
		<?php endif; ?>

		<?php if (is_post_type_archive('fly') && !empty ($page_title_fly) ) : ?>
		<h1 class="page__title"><?php echo $page_title_fly; ?></h1>
		<?php elseif (is_post_type_archive('fly') && empty($page_title_fly) ) : ?>
		<h1 class="page__title"><?php echo $title; ?></h1>
		<?php endif; ?>

		<?php if (is_post_type_archive('job') && !empty ($page_title_job) ) : ?>
		<h1 class="page__title"><?php echo $page_title_job; ?></h1>
		<?php elseif (is_post_type_archive('job') && empty($page_title_job) ) : ?>
		<h1 class="page__title"><?php echo $title; ?></h1>
		<?php endif; ?>

		<?php if (is_post_type_archive('begivenhed') && !empty ($page_title_event) ) : ?>
		<h1 class="page__title"><?php echo $page_title_event; ?></h1>
		<?php elseif (is_post_type_archive('begivenhed') && empty($page_title_event) ) : ?>
		<h1 class="page__title"><?php echo $title; ?></h1>
		<?php endif; ?>
		
		<?php if (!is_post_type_archive('begivenhed') ) : ?>
		<a class="page__cta" href="<?php echo esc_url($link); ?>">
			<div class="page__cta-wrap">
				<img class="page__cta-img" src="<?php echo get_template_directory_uri(); ?>/assets/img/flight-icon.png" alt="icon">
				<h3 class="page__cta-title"><?php echo $text; ?></h3>
			</div>
		</a>
		<?php endif; ?>

	</div>

</section>
