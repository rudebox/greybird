<?php 
//maps settings

// Prepare
$attributes = '';

// Zoom
$zoom = get_field('maps_zoom', 'options') ? : 12;
$attributes .= " data-zoom='$zoom'";

// Maps settings
if ( $settings = get_field('maps_settings', 'options') ) {
    foreach ( $settings as $setting ) {
        $attributes .= " data-$setting='true'";
    }
}

/*
* Marker
*/

// Color
if ( $marker_color = get_field('marker_color', 'options') ) {
    $attributes .= "data-color='$marker_color'";
}

// Fillopacity, used for inner color too
$marker_fillopacity = get_field('marker_fillopacity', 'options');
    if ( $marker_fillopacity ) {
    $attributes .= "data-fillopacity='$marker_fillopacity'";
}

// Inner color
$marker_color_inner = get_field('marker_color_inner', 'options');
    if ( $marker_color_inner && $marker_fillopacity == true ) {
    $attributes .= "data-color-inner='$marker_color_inner'";
}

// Strokeweight
if ( $marker_strokeweight = get_field('marker_strokeweight', 'options') ) {
 $attributes .= "data-strokeweight='$marker_strokeweight'";
}

// Marker size
if ( $marker_size = get_field('marker_size', 'options') ) {
    $attributes .= "data-size='$marker_size'";
}

// Custom marker
$marker = get_field('maps_marker', 'options');
    if ( $marker && get_field('add_custom_marker', 'options') ) {
    $attributes .= ' data-marker="' . $marker['sizes']['thumbnail'] . '"';
}

// Snazzymap
if ( $snazzy = get_field('add_snazzy', 'options') ) {
    $snazzymap = get_field('snazzymap', 'options');
    $attributes .= " data-styles='$snazzymap'";
}
 ?>

    <div class="google-map google-map--footer js-maps" <?php echo $attributes; ?>>

        <?php
            //google map    
            $location = get_field('footer_google_maps', 'options');
            $location['address'];
            $address = get_field('address') ?: $location['address'];
         ?>
            
            <div class="marker" data-lat="<?php echo $location['lat']; ?>" data-lng="<?php echo $location['lng']; ?>" data-icon="<?php echo $icon['url']; ?>">
                <p><?php echo esc_html($address); ?></p>
            </div>

    </div>