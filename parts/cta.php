<?php 
/**
* Description: Lionlab CTA field group layout
*
* @package Lionlab
* @subpackage Lionlab
* @since Version 1.0
* @author Kaspar Rudbech
*/

$img = get_field('cta_img', 'options');
$title = get_field('cta_title', 'options');
$link = get_field('cta_link', 'options');
$link_text = get_field('cta_link_text', 'options');
?>

<div class="cta--wrapper bg--orange">
	<section class="cta">

		<div class="cta__bg parallax" data-rellax-speed="2" data-rellax-percentage="0.5">
			<img src="<?php echo get_template_directory_uri(); ?>/assets/img/skies_3.png" alt="">
		</div>

		<div class="wrap hpad">
			<div class="row flex flex--wrap">

				<div class="cta__item cta__item--text col-sm-6">
					<h2 class="cta__title"><?php echo $title; ?></h2>
					<a class="btn btn--white" href="<?php echo esc_url($link); ?>"><?php echo esc_html($link_text); ?></a>
				</div>

				<div class="cta__item cta__item--img col-sm-6" style="background-image: url(<?php echo esc_url($img['url']); ?>);">
				</div>

			</div>
		</div>
	</section>
</div>	