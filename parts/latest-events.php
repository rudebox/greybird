<?php 
//get current date to compare with event date
$today = date('Y-m-d');

// Custom WP query query
$args_query = array(
  'post_type' => array('begivenhed'),
  'posts_per_page' => 4,
  'order' => 'ASC',
  'orderby' => 'meta_value',
  'meta_query' => array(
        array(
            'key' => 'event_date',
            'value' => $today,
            'compare' => '>='
        )
    ) 
);

$link = get_field('latest_events_link', 'options');

$query = new WP_Query( $args_query );

?>

<section class="events padding--both">
  <div class="wrap hpad">

    <h2 class="events__header"><?php _e('Kommende begivenheder', 'lionlab'); ?></h2>

    <div class="row flex flex--wrap">

        <?php 
            if ($query->have_posts() ) : while ($query->have_posts()): $query->the_post(); 

              //get thumbnail
              $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'link-boxes' );

              //post img alt tag
              $alt = get_post_meta($thumb, '_wp_attachment_image_alt', true);  


              //ACF variables
              $name = get_field('event_name');
              $date = wp_date('j. F Y', strtotime(get_field('event_date')));
              $time = get_field('event_time');
              $location_name = get_field('event_location_name');
              $address = get_field('event_location_address');
              $zip = get_field('event_location_zip');
              $city = get_field('event_location_city');
              $info_meeting = get_field('event_infomeeting');
              $full = get_field('event_full');
              $seats = get_field('event_seats');
              $close = get_field('event_close');
              $hide_date_time = get_field('event_hide_date_time');

              //set timeze to get local timezone
              $start_time = strtotime('-2 hours', strtotime(get_field('event_start_time')));
              date_default_timezone_set('Europe/Copenhagen');
        ?>

        <a href="<?php the_permalink(); ?>" class="events__item col-xs-6 col-md-3 anim fade-up" itemscope itemtype="http://schema.org/Event">

           <div class="events__wrap">
            <?php if ($full === true || $start_time <= time() ) : ?>
              <div class="events__status events__status--booked"><?php _e('Fuldt booket', 'lionlab') ?></div>
            <?php endif; ?>

            <?php if ($hide_date_time === false) : ?>
            <div class="events__details" itemprop="startDate">
                <span class="events__details-date orange"><?php echo esc_html($date); ?></span><br>
                <span class="events__details-time white"><?php echo esc_html($time); ?></span>
            </div>
            <?php endif; ?>

            <?php if (!$thumb) : ?>
              <?php if ($info_meeting === false) : ?>
              <img class="events__img" src="<?php echo get_template_directory_uri(); ?>/assets/img/begivenhed.jpg" alt="greybird_event" loading="lazy">
              <?php endif; ?>
              <?php if ($info_meeting === true) : ?>  
              <img class="events__img" src="<?php echo get_template_directory_uri(); ?>/assets/img/infomeeting_greybird.jpg" alt="greybird_event" loading="lazy"> 
              <?php endif; ?>       
            <?php else : ?>
            <img class="events__img" src="<?php echo esc_url($thumb[0]); ?>" alt="<?php echo esc_attr($alt); ?>" loading="lazy">   
            <?php endif; ?>      
          </div>

          <header class="flights__stats">
            <h2 class="h5 orange" itemprop="name" title="<?php echo esc_html($name); ?>">
              <?php echo esc_html($name); ?> 
            </h2>

            <?php the_excerpt(); ?>

            <div class="events__location" itemprop="location" itemscope itemtype="http://schema.org/Place">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/event-map-icon.png" alt="map-icon"> 
                <p itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
                  <span class="events__location-adress" itemprop="name"><?php echo esc_html($location_name); ?></span>
                  <span class="events__location-adress" itemprop="streetAddress"><?php echo esc_html($address); ?></span>
                  <span itemprop="postalCode"><?php echo esc_html($zip); ?></span>
                  <span itemprop="addressLocality"><?php echo esc_html($city); ?></span>
                </p>
            </div>

          </header>

          <span class="btn btn--ticket"><?php _e('Læs mere', 'lionlab') ?></span>

        </a>

        <?php endwhile; wp_reset_postdata(); else: ?>
        
        <div class="wrap hpad">
          <p><?php _e('Der er ingen indlæg i denne kategori at vise på nuværende tidspunkt', 'lionlab') ?></p>
        </div>

        <?php endif; ?>

    </div>

    <div class="events__btn">
      <a class="btn btn--orange" href="<?php echo esc_url($link); ?>"><?php _e('Se flere begivenheder', 'lionlab'); ?></a>
    </div>

  </div>
</section>

