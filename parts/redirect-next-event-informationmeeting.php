<?php

/*
 * Template Name: Redirect til næste informationsmøde
 */

//redirect to next danish info meeting 

// Custom WP query query

$args = array(
	'post_type' => 'begivenhed',
	'suppress_filters' => false,
	'posts_per_page' => 1,
	'post_status' => array('publish'),
	'order' => 'ASC',
  	'orderby' => 'meta_value', 
  	'meta_query' => array(
        array(
            'key' => 'event_date',
            'value' => $today,
            'compare' => '>='
        ),
        array(
			'key'	=> 'event_infomeeting',
			'value'	=> true
		)
    )
);

//fetch posts that match and grab the URL of the first post
$posts = get_posts($args, );

$redirect_url = get_permalink($posts[0]->ID);

//redirect to the specified URL
wp_safe_redirect($redirect_url);

exit;

?>