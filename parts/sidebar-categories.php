<aside class="sidebar sidebar--categories bg--white center">
  <?php 
    //get top level categories
    $categories = get_categories( array(
        'orderby' => 'name',
        'parent'  => 0,
        'exclude' => '13'
    ) );

    $category_id = $categories->term_id;
  ?>

  <h4 class="sidebar__category-header"><?php _e('Kategorier', 'lionlab') ?></h4>

  <div id="filters">
    <button class="sidebar__category-links is-active" data-filter="*"><?php _e('Alle', 'lionlab') ?></button>

    <?php foreach ($categories as $category) : ?>
    	<button class="sidebar__category-links" data-filter=".cat--<?php echo $category->term_id;?>"><?php echo $category->name; ?></button>
  	<?php endforeach; ?>
  </div>

	<form class="search__form " method="get" autocomplete="off" action="<?php bloginfo('url'); ?>/">
		<input class="search__input" type="text" value="<?php the_search_query(); ?>" placeholder="<?php _e('Indtast søgeord', 'lionlab'); ?>" name="s" id="s"></input> 
		<button type="submit" class="search__btn"><i class="fas fa-search"></i> </button>
	</form>
</aside>