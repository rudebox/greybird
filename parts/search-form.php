<?php 
  //get top level categories
  $categories = get_categories( array(
      'orderby' => 'name',
      'parent'  => 0
  ) );


?> 

<section class="search--bar bg--orange">
	<div class="wrap hpad flex flex--wrap flex--valign" id="filters"> 

  	<?php foreach ($categories as $category) : ?>
  		<button class="search__category white" data-filter=".cat--<?php echo $category->term_id;?>"><strong><?php echo $category->name; ?></strong></button>
		<?php endforeach; ?>

		<form class="search__form search__form--bar" method="get" autocomplete="off" action="<?php bloginfo('url'); ?>/">
			<input class="search__input" type="text" value="<?php the_search_query(); ?>" placeholder="<?php _e('Indtast søgeord', 'lionlab'); ?>" name="s" id="s"></input> 
			<button type="submit" class="search__btn"><i class="fas fa-search"></i> </button>
		</form>

	</div>
</section>