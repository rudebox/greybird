<?php 
// Custom WP query query
$args_query = array(
	'posts_per_page' => 6,
	'order' => 'DESC',
	'post_type' => 'job'
);

$query = new WP_Query($args_query);

if ($query->have_posts() ) :
?>

<aside class="sidebar sidebar--articles">

	<h4 class="sidebar__articles-title orange"><?php _e('Ledige jobs', 'lionlab') ?></h4>

	<?php 
		while ($query->have_posts() ) : $query->the_post(); 

		//get thumbnail
		$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'url' );

		//get category name of post
        $categories = get_the_category();   

        //job variables
		$place = get_field('job_place');
		$type = get_field('job_type');
	?>


	<a href="<?php echo the_permalink(); ?>" class="blog__post blog__post--sidebar" itemscope itemtype="http://schema.org/JobPosting">

	  <header>
	    <img class="blog__img" src="<?php echo esc_url($thumb[0]); ?>" alt="">
	    <span class="blog__meta blog__meta--sidebar">
	    	<time datetime="<?php the_time('c'); ?>"><?php the_time('d/m/Y'); ?></time>
	        <?php if ($type) : ?>
	          <span><i class="far fa-clock"></i> <?php echo esc_html($type); ?></span> 
	        <?php endif; ?> 
        </span>
	    <h5 class="blog__title" title="<?php the_title_attribute(); ?>" itemprop="title">
	      <?php the_title(); ?>
	    </h5>

	  </header>

	  <div itemprop="description">
	    <?php echo wp_trim_words(get_the_excerpt(), 10, '...'); ?> 
	  </div>

	</a>

<?php endwhile; wp_reset_postdata(); else: ?>

	<p><?php _e('Der er ingen indlæg i denne kategori at vise på nuværende tidspunkt', 'lionlab') ?></p>

</aside>

<?php endif; ?>
