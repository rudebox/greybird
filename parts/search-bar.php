<aside class="sidebar sidebar--categories bg--white center">
    <?php 
      //get top level categories
      $categories = get_categories( array(
          'orderby' => 'name',
          'parent'  => 0
      ) );
    ?>

    <h4 class="sidebar__category-header"><?php _e('Kategorier', 'lionlab') ?></h4>

    <?php foreach ($categories as $category) : ?>
    	<a class="sidebar__category-links" href="<?php echo get_category_link($category->cat_ID); ?>"><?php echo $category->name; ?></a>
  	<?php endforeach; ?>

	<form class="search search__form " method="get" autocomplete="off" action="<?php bloginfo('url'); ?>/">
		<input class="search__input" type="text" value="<?php the_search_query(); ?>" placeholder="<?php _e('Indtast søgeord', 'lionlab'); ?>" name="s" id="s"></input> 
		<button type="submit" class="search__btn"><i class="fas fa-search"></i> </button>
	</form>
</aside>