<?php

/* Do not remove this line. */
require_once('includes/scratch.php');
require_once('lib/theme-dependencies.php');

define('GOOGLE_MAPS_KEY', 'AIzaSyBcFVnpkXT6enxD9JwpgZzzrCyVY6Qu3f4');


/*
 * adds all meta information to the <head> element for us.
 */

function scratch_meta() { ?>

  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="description" content="<?php bloginfo('description'); ?>">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <link rel="apple-touch-icon" href="/apple-touch-icon.png">
  <!-- Place favicon.ico in the root directory -->

<?php }

add_action('wp_head', 'scratch_meta');

/* Theme CSS */

function theme_styles() { 

  wp_enqueue_style( 'fontawesome', 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.9.0/css/all.min.css', false, null );

  wp_register_style( 'scratch-main', get_template_directory_uri() . '/assets/css/master.css', false, filemtime(dirname(__FILE__) . '/assets/css/master.css') );
  wp_enqueue_style( 'scratch-main' );

}

add_action( 'wp_enqueue_scripts', 'theme_styles' );

/* Theme JavaScript */

function theme_js() {

  if (!is_admin()) {
    wp_deregister_script( 'jquery' );
    wp_register_script( 'jquery', 'https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js', array(), null, true );
  }

  wp_register_script( 'main-concat', get_template_directory_uri() . '/assets/js/concat/main.js', array('jquery'), filemtime(dirname(__FILE__) . '/assets/js/concat/main.js'), true ); 

  wp_register_script( 'main-min', get_template_directory_uri() . '/assets/js/build/main.min.js', array('jquery'), filemtime(dirname(__FILE__) . '/assets/js/build/main.min.js'), true );

  if ( defined( 'GOOGLE_MAPS_KEY' ) )  {
    wp_enqueue_script( 'google-maps', 'https://maps.googleapis.com/maps/api/js?key=' . GOOGLE_MAPS_KEY,array('jquery'), null, true ); 
  }

  wp_enqueue_script( 'markercluster', 'https://unpkg.com/@google/markerclustererplus@4.0.1/dist/markerclustererplus.min.js', array(), null, true );

  /* FOR DEVELOPMENT */
  // wp_enqueue_script( 'main-concat' );

  /* FOR PRODUCTION */
  wp_enqueue_script( 'main-min' );

}

add_action( 'wp_enqueue_scripts', 'theme_js' );


// Add Google Maps API
if ( defined( 'GOOGLE_MAPS_KEY' ) && GOOGLE_MAPS_KEY !== null ) {
  function add_google_maps_key_to_acf( $api ) {
      $api['key'] = GOOGLE_MAPS_KEY;
      return $api;
  } 
  add_filter('acf/fields/google_map/api', 'add_google_maps_key_to_acf');
} 


//defer JS
function defer_parsing_of_js( $url ) {
    if ( is_user_logged_in() ) return $url; //don't break WP Admin
    if ( FALSE === strpos( $url, '.js' ) ) return $url;
    if ( strpos( $url, 'jquery.min.js' ) ) return $url;
    return str_replace( ' src', ' defer src', $url );
}

add_filter( 'script_loader_tag', 'defer_parsing_of_js', 10 );


/* Enable ACF Options Pages */


if( function_exists('acf_add_options_page') ) {
  
  acf_add_options_page(array(
    'page_title'  => 'Globalt indhold',
    'menu_title'  => 'Globalt indhold',
    'menu_slug'   => 'global-content',
    'capability'  => 'edit_posts',
    'redirect'    => false
  ));
  
  acf_add_options_sub_page(array(
    'page_title'  => 'Header',
    'menu_title'  => 'Header',
    'parent_slug' => 'global-content',
  ));

  acf_add_options_sub_page(array(
    'page_title'  => 'Sidebar',
    'menu_title'  => 'Sidebar',
    'parent_slug' => 'global-content',
  ));
  
  acf_add_options_sub_page(array(
    'page_title'  => 'Footer',
    'menu_title'  => 'Footer',
    'parent_slug' => 'global-content',
  ));

  acf_add_options_sub_page(array(
      'page_title'      => 'Jobs Arkiv indstillinger',
      'parent_slug'     => 'edit.php?post_type=job', 
      'capability' => 'manage_options'
  ));

  acf_add_options_sub_page(array(
      'page_title'      => 'Fly Arkiv indstillinger',
      'parent_slug'     => 'edit.php?post_type=fly', 
      'capability' => 'manage_options'
  ));

  acf_add_options_sub_page(array(
      'page_title'      => 'Testimonials Arkiv indstillinger',
      'parent_slug'     => 'edit.php?post_type=testimonial', 
      'capability' => 'manage_options'
  ));

  acf_add_options_sub_page(array(
      'page_title'      => 'Begivenheder Arkiv indstillinger',
      'parent_slug'     => 'edit.php?post_type=begivenhed', 
      'capability' => 'manage_options' 
  ));
}


/* Enable Featured Image */
add_theme_support( 'post-thumbnails' );

// Image sizes
add_image_size('avatar', 80, 80, true);
add_image_size('link-boxes', 800, 533, true);
add_image_size('flights', 400, 225, true);

// Add created images sizes to dropdown in WP control panel
add_filter( 'image_size_names_choose', 'custom_image_sizes' );

function custom_image_sizes( $sizes ) {
  return array_merge( $sizes, array( 
    
  ) );
} 

/* Enable Custom Menus */

add_theme_support( 'menus' );

register_nav_menus(
  array(
    'main-nav' => __( 'Main Nav', 'lionlab' ),   // main nav in header
    'social-nav' => __( 'Social Nav', 'lionlab' ),   // social nav in header
    'lang-nav' => __( 'lang Nav', 'lionlab' )   // social nav in header
  )
);

function main_nav() {
  // display the wp3 menu if available
  wp_nav_menu(array(
    'container' => false, // remove nav container
    'container_class' => '', // class of container (should you choose to use it)
    'menu' => __( 'Main Nav', 'lionlab' ), // nav name
    'menu_class' => 'nav__menu', // adding custom nav class
    'theme_location' => 'main-nav', // where it's located in the theme
    'before' => '', // before the menu
    'after' => '', // after the menu
    'link_before' => '', // before each link
    'link_after' => '', // after each link
    'depth' => 0,    // fallback function
    'walker' => new Clean_Walker_Nav () //remove wordpress walker
  ));
} /* end scratch main nav */


function lang_nav() {
  // display the wp3 menu if available
  wp_nav_menu(array(
    'container' => false, // remove nav container
    'container_class' => '', // class of container (should you choose to use it)
    'menu' => __( 'lang Nav', 'lionlab' ), // nav name
    'menu_class' => 'nav__menu nav__menu--lang', // adding custom nav class
    'theme_location' => 'lang-nav', // where it's located in the theme
    'before' => '', // before the menu
    'after' => '', // after the menu
    'link_before' => '', // before each link
    'link_after' => '', // after each link
    'depth' => 0,    // fallback function
    'walker' => new Clean_Walker_Nav () //remove wordpress walker
  ));
} /* end scratch social nav */


function social_nav() {
  // display the wp3 menu if available
  wp_nav_menu(array(
    'container' => false, // remove nav container
    'container_class' => '', // class of container (should you choose to use it)
    'menu' => __( 'Social Nav', 'lionlab' ), // nav name
    'menu_class' => 'nav__menu nav__menu--social', // adding custom nav class
    'theme_location' => 'social-nav', // where it's located in the theme
    'before' => '', // before the menu
    'after' => '', // after the menu
    'link_before' => '', // before each link
    'link_after' => '', // after each link
    'depth' => 0,    // fallback function
    'walker' => new Clean_Walker_Nav () //remove wordpress walker
  ));
} /* end scratch social nav */


//scroll to form on submit
add_filter( 'gform_confirmation_anchor', '__return_true' );


//accordion shortcode
function fold_shortcode( $atts , $content = null ) {

    $atts = shortcode_atts(
      array(
        'titel' => '',
        'class' => '',
        'visible' => '',
      ),
      $atts
    );

  return '<div class="accordion__wrapper anim fade-up"><h3 class="accordion__title ' . $atts['class'] . '">' . $atts['titel'] . '<img src="' . get_template_directory_uri() . '/assets/img/dropdown-arrow-grey.png" class="icon"></h3><div class="accordion__panel ' . $atts['visible'] . '">' . $content . '</div></div>';

}

add_shortcode( 'fold', 'fold_shortcode' );


//sidebar shortcode
function sidebar_shortcode( $atts , $content = null ) {

    $atts = shortcode_atts(
      array(
        'titel' => '',
      ),
      $atts
    );

  return '<aside class="sidebar sidebar--content">' . $content . '</aside>';

}

add_shortcode( 'sidebar', 'sidebar_shortcode' );


//youtube shortcode
function yt_shortcode( $atts ) {

  $atts = shortcode_atts(
    array(
      'url' => '',
    ),
    $atts
  );

  //trim url for easier string replacement
  $trim_url = parse_url($atts['url'], PHP_URL_PATH);

  //replace and strip string from videolink variable down to video ID for thumbnail use
  $video_id = str_replace('/embed/', '', $trim_url);

  //get icon url
  $icon = file_get_contents('wp-content/themes/greybird/assets/img/youtube.svg');


  return '<div class="embed"><a class="embed__item" style="background-image: url(https://img.youtube.com/vi/' . $video_id . '/maxresdefault.jpg);" data-fancybox data-type="iframe" data-src="' . $atts['url'] . '?autoplay=1&showinfo=0&rel=0" href="javascript:;"><i class="embed__play">' . $icon . '</i></a>
    </div>';

  $output = ob_get_clean();
  echo $output;

}

add_shortcode( 'yt_shortcode', 'yt_shortcode' );


//remove tabindex for GF
add_filter( 'gform_tabindex', '__return_false' );


//change canonical URL because custom post type archives canonical URL's not being translated properly - workaround
function ps_change_cannonical($url) {

    $url = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

    return $url;

} 

add_filter('wpseo_canonical' , 'ps_change_cannonical', 10 ,1);


require_once('lib/gform-list-field.php');

//redirect when events expire
add_action( 'template_redirect', 'unlisted_events_redirect' );
function unlisted_events_redirect(){
    //check for 404
    if( is_404()) {
        global $wp_query;
        //check that wp has figured out post_type from the request
        //and it's the type you're looking for
        if( isset($wp_query->query['post_type']) && $wp_query->query['post_type'] == 'begivenhed' ){
          // then redirect to given URL
          if (ICL_LANGUAGE_CODE === 'sv') {  
            wp_safe_redirect( home_url( '/sv/events/' ) );
          }
          elseif (ICL_LANGUAGE_CODE === 'en') {
            wp_safe_redirect( home_url( '/en/events/' ) );
          }
          else {
            wp_safe_redirect( home_url( '/begivenheder/' ) );
          }
        exit();
    }
  }
}

/* Place custom functions below here. */

/* Don't delete this closing tag. */

?>
