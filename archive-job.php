<?php 
  get_template_part('parts/header'); 

  $text = get_field('job_text', 'options');
?>

<main>

<?php get_template_part('parts/page', 'header-cpt'); ?>


  <div class="page__wrap">

    <div class="page__bg page__bg--1 parallax">
      <img src="<?php echo get_template_directory_uri(); ?>/assets/img/bg_skies_3.png" alt="skies_grey_bird">
    </div>

    <div class="page__bg page__bg--2 parallax">
      <img src="<?php echo get_template_directory_uri(); ?>/assets/img/bg_skies_2.png" alt="skies_grey_bird">
    </div>

    <div class="page__bg page__bg--3 parallax">
      <img src="<?php echo get_template_directory_uri(); ?>/assets/img/bg_skies.png" alt="skies_grey_bird">
    </div>

    <section class="jobs padding--both">
      <div class="wrap hpad">
        
        <?php echo $text; ?>

        <div class="row--fluid flex flex--wrap">

          <?php if (have_posts()): ?>
            <?php while (have_posts()): the_post(); 

              //get thumbnail
              $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'url' );

              //job variables
              $place = get_field('job_place');
              $type = get_field('job_type');
            ?>

            <div class="search__item anim fade-up" itemscope itemtype="http://schema.org/JobPosting">
              
              <div class="col-sm-8 search__content">
                <header>
                  <div class="blog__meta blog__meta--job orange">

                    <?php if ($type) : ?>
                      <span><i class="far fa-clock"></i> <?php echo esc_html($type); ?></span> 
                    <?php endif; ?>

                    <?php if ($place) : ?>
                      <span itemprop="addressLocality"><i class="fas fa-map-marker-alt"></i> <?php echo esc_html($place); ?></span> 
                    <?php endif; ?>
                  </div>

                  <h2 class="search__title" itemprop="title" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></h2>
                </header>

                <div itemprop="description">
                  <?php the_excerpt(); ?>

                  <a class="btn btn--orange" href="<?php the_permalink(); ?>"><?php _e('Læs mere', 'lionlab') ?></a>
                </div>
              </div>
              
              <?php if ($thumb) : ?>
                <div class="search__thumbnail col-sm-4" style="background-image: url(<?php echo esc_url($thumb[0]); ?>)"></div>
              <?php endif; ?>

            </div>

            <?php endwhile; else: ?>

              <p><?php _e('Der er ingen jobs tilgængelig i øjeblikket', 'lionlab') ?></p>

          <?php endif; ?>

        </div>
      </div>
    </section>

  </div>

  <?php get_template_part('parts/cta'); ?>

</main>

<?php get_template_part('parts/footer'); ?>