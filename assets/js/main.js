jQuery(document).ready(function($) {

  var rellax = new Rellax('.parallax', {
    wrapper: null
  });

  //in viewport check
  var $animation_elements = $('.anim');
  var $window = $(window);

  function check_if_in_view() {
    var window_height = $window.height();
    var window_top_position = $window.scrollTop();
    var window_bottom_position = (window_top_position + window_height);
   
    $.each($animation_elements, function() {
      var $element = $(this);
      var element_height = $element.outerHeight();
      var element_top_position = $element.offset().top;
      var element_bottom_position = (element_top_position + element_height);
   
      //check to see if this current container is within viewport
      if ((element_top_position <= window_bottom_position)) {
        $element.addClass('in-view');
      } else {
        $element.removeClass('in-view');
      }
    }); 
  }

  $window.on('scroll resize', check_if_in_view);
  $window.trigger('scroll');

  //menu toggle
  $('.nav-toggle').click(function(e) {
    e.preventDefault();
    $('.nav--mobile').toggleClass('is-open');
    $('body').toggleClass('is-fixed');
    $('.nav__item').toggleClass('is-animated'); 
  });


  //accordion
  var $acc = $('.accordion__title'); 

    $acc.each(function() {
      $(this).click(function() {
        $(this).toggleClass('is-active');
        $(this).next().toggleClass('is-visible');
    });
  });

  //wrap embeds
  $('iframe[src*="youtube"]').wrap("<div class='embed'></div>");
  $('.fb_embed').wrap("<div class='embed'></div>"); 


  //fancybox
  $('[data-fancybox]').fancybox({ 
    toolbar  : false,
    smallBtn : true,
    iframe : { 
      preload : false 
    }
  });

  //wrap table for responsiveness
  $('.table').wrap('<div class="table-wrapper">');

  //share toggle
  $('.share__toggle').on('click', function() {

    
    $('.share__link').each(function(index) {        
        var delayNumber = index * 100;
        
        $(this).delay(delayNumber).queue(function(next) {
          $(this).toggleClass('is-active');
          next();
        });  

    })
  });


  //ative class sidebar categories
  $('.sidebar__category-links').each(function() {   
      if (this.href == window.location.href) {
          $(this).addClass('is-active');
      }
  });



  //mixit up
  if($('body').is('.blog')) {

    var itemSelector = '.blog__post'; 

    var $grid = $('.blog__grid').isotope({
      itemSelector: '.blog__post',
      percentPosition: true,
      masonry: {
        columnWidth: '.col-sm-6'
      }
    });

    // bind filter button click
    $('#filters').on( 'click', 'button', function() {
      var filterValue = $( this ).attr('data-filter');

      $grid.isotope({ filter: filterValue });
    });

    $('#filters button').on('click', function(){
      $('#filters button.is-active').removeClass('is-active');
      $(this).addClass('is-active');
    });


    //Ascending order
    var responsiveIsotope = [
        [480, 7],
        [720, 7]
    ];

    var itemsPerPageDefault = 7;
    var itemsPerPage = defineItemsPerPage();
    var currentNumberPages = 1;
    var currentPage = 1;
    var currentFilter = '*';
    var filterAtribute = 'data-filter';
    var pageAtribute = 'data-page';
    var pagerClass = 'isotope-pager';

    function changeFilter(selector) {
        $grid.isotope({
            filter: selector
        });
    }


    function goToPage(n) {
        currentPage = n;

        var selector = itemSelector;
            selector += ( currentFilter != '*' ) ? '['+filterAtribute+'="'+currentFilter+'"]' : '';
            selector += '['+pageAtribute+'="'+currentPage+'"]';

        changeFilter(selector);
    }

    function defineItemsPerPage() {
        var pages = itemsPerPageDefault;

        for( var i = 0; i < responsiveIsotope.length; i++ ) {
            if( $(window).width() <= responsiveIsotope[i][0] ) {
                pages = responsiveIsotope[i][1];
                break;
            }

        }

        return pages;
    }

    function setPagination() {

        var SettingsPagesOnItems = function(){

            var itemsLength = $grid.children(itemSelector).length;

            var pages = Math.ceil(itemsLength / itemsPerPage);
            var item = 1;
            var page = 1;
            var selector = itemSelector;
                selector += ( currentFilter != '*' ) ? '['+filterAtribute+'="'+currentFilter+'"]' : '';

            $grid.children(selector).each(function(){
                if( item > itemsPerPage ) {
                    page++;
                    item = 1;
                }
                $(this).attr(pageAtribute, page);
                item++;
            });

            currentNumberPages = page;

        }();

        var CreatePagers = function() {

            var $isotopePager = ( $('.'+pagerClass).length == 0 ) ? $('<div class="'+pagerClass+'"></div>') : $('.'+pagerClass);

            $isotopePager.html('');

            for( var i = 0; i < currentNumberPages; i++ ) {
                var $pager = $('<a href="javascript:void(0);" class="pager" '+pageAtribute+'="'+(i+1)+'"></a>');
                    $pager.html(i+1);

                    $pager.click(function(){
                        var page = $(this).eq(0).attr(pageAtribute);
                        goToPage(page);
                    });

                $pager.appendTo($isotopePager);
            }

            $grid.after($isotopePager);

        }();

    }

    setPagination();
    goToPage(1);

    //Adicionando Event de Click para as categorias
    $('#filters > button').click(function(){
        var filter = $(this).attr(filterAtribute);
        currentFilter = filter;

        setPagination();
        goToPage(1);

    });

    //Evento Responsivo
    $(window).resize(function(){
        itemsPerPage = defineItemsPerPage();
        setPagination();
        goToPage(1);
    });

  }

  //Show error message if attendes number is higher than available seats and show no message error if available seats equals -1
  var val = $('.gf_seats input').val();

  if(parseInt(val) != -1) {

    $('input').keyup(function() {
      var val1 = $('.gf_attendees input').val();
      var val2 = $('.gf_seats input').val();

      
     if(parseInt(val1) > parseInt(val2) ) {
      $('.gf-error-message').css({
        display: 'block'        
      });
        $('.gform_button').prop('disabled', true);
        $('.gform_button').css({
        cursor: 'not-allowed'
      });
     } 
     else {
       $('.gf-error-message').css({
            display: 'none'        
              });
          $('.gform_button').prop('disabled', false);
          $('.gform_button').css({
          cursor: 'pointer'
        });
     }

    }).keyup();
    } else {
      $('.gf-error-message').css({
            display: 'none'        
      });
    }


  //owl slider/carousel
  var owl = $(".slider__track");

  owl.owlCarousel({
      loop: true,
      items: 1,
      autoplay: true,
      nav: true,
      dots: true,
      autplaySpeed: 11000,
      autoplayTimeout: 10000,
      smartSpeed: 250,
      smartSpeed: 2200,
      navSpeed: 2200,
      navText : ["<img class='owl-btn' src='/wp-content/themes/greybird/assets/img/btn_slider.png'>", "<img class='owl-btn' src='/wp-content/themes/greybird/assets/img/btn_slider.png'>"]
  });

});
