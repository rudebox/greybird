<?php 
  get_template_part('parts/header');
  get_template_part('parts/page', 'header'); 
?>

<main itemscope itemtype="http://schema.org/SearchResultsPage">

  <section class="search padding--both">
    <div class="wrap hpad">

      <p class="center">
          <strong> <?php _e('Søgeresultat for', 'lionlab') ?>:</strong>
          <?php echo esc_attr(get_search_query()); ?>
      </p>

      <div class="row--fluid flex flex--wrap">

          <?php if (have_posts()): ?>
            <?php while (have_posts()): the_post(); 

                //get thumbnail
                $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'url' );
            ?>
  
            <article class="search__item col-sm-12 flex anim fade-up">
              
              <div class="search__content col-sm-8">
                <header>
                  <h2 class="search__title" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></h2>
                </header>

                <div itemprop="description">
                  <?php the_excerpt(); ?>
                  <a class="btn btn--orange" href="<?php the_permalink(); ?>"><?php _e('Læs mere', 'lionlab') ?></a>
                </div>
              </div>
              
              <?php if ($thumb) : ?>
              <div class="search__thumbnail col-sm-4" style="background-image: url(<?php echo esc_url($thumb[0]); ?>)"></div>
              <?php endif; ?>

            </article>

            <?php endwhile; else: ?>

              <p class="center"><?php _e('Din søgning for', 'lionlab'); ?> <strong><?php echo esc_attr(get_search_query()); ?></strong> <?php _e('gav ingen resultater', 'lionlab'); ?>.</p>

          <?php endif; ?>

          <?php 
              do_action( 'lionlab_pagination' );
           ?>

      </div>
    </div>

  </section>
</main>

<?php get_template_part('parts/footer'); ?>