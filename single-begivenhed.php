<?php

get_template_part('parts/header'); the_post();


//Event details variables
$name = get_field('event_name');
$date = date('d/m/Y', strtotime(get_field('event_date')));
$time = get_field('event_time');
$adress = get_field('event_adress');
$ev_id = get_field('event_id');
$info_meeting = get_field('event_infomeeting');
$seats = get_field('event_seats');
$city = get_field('event_location_city');
$full = get_field('event_full');
$close = get_field('event_close');
$hide_date_time = get_field('event_hide_date_time'); 
$event_form = get_field('event_form');  

$link = get_field('latest_events_link', 'options');

//Note: when changing choice values, we also need to use the gform_admin_pre_render so that the right values are displayed when editing the entry.

//prepoplulate event_seats field to check for available seats on each event
$field = 'event_seats';
add_filter('gform_field_value_'.$field, 'populate_field');

function populate_field($value){
  return  get_field('event_seats');
}

?>

<main>
    <div class="page__wrap">

    <div class="page__bg page__bg--1 parallax">
      <img src="<?php echo get_template_directory_uri(); ?>/assets/img/bg_skies_3.png" alt="skies_grey_bird">
    </div>

    <div class="page__bg page__bg--2 parallax">
      <img src="<?php echo get_template_directory_uri(); ?>/assets/img/bg_skies_2.png" alt="skies_grey_bird">
    </div>

    <div class="page__bg page__bg--3 parallax">
      <img src="<?php echo get_template_directory_uri(); ?>/assets/img/bg_skies.png" alt="skies_grey_bird">
    </div>

    <section class="single padding--both">
      <div class="wrap hpad">
        <div class="row events__row">
            
            <div class="col-sm-4">
              <aside class="sidebar sidebar--newsletter bg--grey-dark">

                <?php 
                  //set timeze to get local timezone
                  $start_time = strtotime('-2 hours', strtotime(get_field('event_start_time')));
                  date_default_timezone_set('Europe/Copenhagen');

                  if ($full === true || $start_time <= time()) : 
                  ?>

                  <div class="single__form">
                    <h3><?php _e('Tilmelding', 'lionlab') ?></h3>
                    <p><?php _e('Denne begivenhed er fuldt booket og lukket for tilmeldinger', 'lionlab') ?></p><br>

                    <a class="btn btn--orange gform_button" href="<?php echo esc_url($link); ?>"><?php _e('Tilbage', 'lionlab') ?></a>
                  </div>

                <?php else : ?>

                 <?php if (ICL_LANGUAGE_CODE === 'sv') : ?>
                  <?php gravity_form( 5, $display_title = true, $display_description = false, $display_inactive = false, $field_values = array('event_id' => $ev_id, 'event_name' => $name, 'event_date' => $date, 'event_time' => $time, 'event_infomeeting' => $info_meeting, 'event_location_city' => $city, 'event_form' => $event_form ), $ajax = false, 2, $echo = true ); ?>

                  <?php elseif (ICL_LANGUAGE_CODE === 'en') : ?>
                  <?php gravity_form( 12, $display_title = true, $display_description = false, $display_inactive = false, $field_values = array('event_id' => $ev_id, 'event_name' => $name, 'event_date' => $date, 'event_time' => $time, 'event_infomeeting' => $info_meeting, 'event_location_city' => $city, 'event_form' => $event_form ), $ajax = false, 2, $echo = true ); ?>

                  <?php else : ?>
                  <?php gravity_form( 3, $display_title = true, $display_description = false, $display_inactive = false, $field_values = array('event_id' => $ev_id, 'event_name' => $name, 'event_date' => $date, 'event_time' => $time, 'event_infomeeting' => $info_meeting, 'event_location_city' => $city, 'event_form' => $event_form ), $ajax = false, 2, $echo = true ); ?>
                  <?php endif; ?>

                <?php endif; ?>
              </aside>
            </div>

            <article class="single__article col-sm-7 col-sm-offset-1" itemscope itemtype="http://schema.org/Event">

              <header class="center"> 
                <h1 class="blog__title orange h3" itemprop="title"><?php echo esc_html($name); ?></h1>

                <?php 
                  //show only available seats if within range of 1 - 3
                  if ($seats >= '1' && $seats <= '3') : 
                ?>
                <div class="events__seats events__seats--count">
                      <strong>
                        <span class="events__seats"><?php _e('Ledige pladser', 'lionlab') ?>:</span><span class="orange"><?php echo esc_html($seats); ?></span>
                      </strong>
                </div>

                <?php endif; ?>

                <span class="blog__meta">
                  <?php if ($hide_date_time === false) : ?>
                    <?php if ($time) : ?>
                          <strong><i class="far fa-clock"></i> <?php echo esc_html($time); ?></strong> 
                    <?php endif; ?>
                    <time datetime="<?php echo esc_attr($date); ?>"><?php echo esc_attr($date); ?></time> 
                  <?php endif; ?>
                  <a class="share__toggle share__toggle--single"><?php _e('Del begivenhed', 'lionlab'); ?> 
                    <i class="far fa-share-square"></i>
                  </a>
                </span>

                <div class="share">
                  <a class="share__link" target="_blank" rel="noopener" href="http://www.facebook.com/sharer/sharer.php?u=<?php the_permalink(); ?>"><i class="fab fa-facebook-f"></i></a>
                  <a class="share__link" target="_blank" rel="noopener" href="https://www.linkedin.com/sharing/share-offsite/?url=<?php echo the_permalink(); ?>"><i class="fab fa-linkedin-in"></i></a>
                  <a class="share__link" target="_blank" rel="noopener" href="https://twitter.com/intent/tweet?url=<?php the_permalink(); ?>"><i class="fab fa-twitter"></i></a>
                  <a class="share__link" href="mailto:?body=<?php the_permalink(); ?>"><i class="fas fa-envelope"></i></a>
                </div>
              </header>

              <div class="single__content" itemprop="description"> 
                <?php the_content(); ?>
              </div>

            </article>

        </div>
      </div>
    </section>

  <?php get_template_part('parts/latest', 'events'); ?>

</div>

</main>

<?php get_template_part('parts/footer'); ?>
