<?php

get_template_part('parts/header'); 

//job variables
$place = get_field('job_place');
$type = get_field('job_type');

?>

<main>

  <section class="single padding--both">
    <div class="wrap hpad">
      <div class="row">

        <?php $sidebar_check = get_field('show_sidebar'); ?>
        
        <?php if ($sidebar_check === true) : ?>
        <div class="col-sm-3">
          <?php get_template_part('parts/sidebar', 'jobs'); ?>
        </div>
        <?php endif;?>
           
          <?php the_post();  ?> 

          <?php if ($sidebar_check === true) : ?>
          <article class="single__article col-sm-8 col-sm-offset-1" itemscope itemtype="http://schema.org/JobPosting">
          <?php else : ?>
          <article class="single__article col-sm-12" itemscope itemtype="http://schema.org/JobPosting">
          <?php endif; ?>


            <header class="center"> 
              <h1 class="blog__title orange h3" itemprop="title"><?php the_title(); ?></h1>
              <span class="blog__meta">
                <?php if ($type) : ?>
                      <strong><i class="far fa-clock"></i> <?php echo esc_html($type); ?></strong> 
                <?php endif; ?>
                <time datetime="<?php the_time('c'); ?>"><?php the_time('d/m/Y'); ?></time> 
                <a class="share__toggle share__toggle--single"><?php _e('Del artiklen', 'lionlab'); ?> 
                  <i class="far fa-share-square"></i>
                </a>
              </span>

              <div class="share">
                <a class="share__link" target="_blank" rel="noopener" href="http://www.facebook.com/sharer/sharer.php?u=<?php the_permalink(); ?>"><i class="fab fa-facebook-f"></i></a>
                <a class="share__link" target="_blank" rel="noopener" href="https://www.linkedin.com/sharing/share-offsite/?url=<?php echo the_permalink(); ?>"><i class="fab fa-linkedin-in"></i></a>
                <a class="share__link" target="_blank" rel="noopener" href="https://twitter.com/intent/tweet?url=<?php the_permalink(); ?>"><i class="fab fa-twitter"></i></a>
                <a class="share__link" href="mailto:?body=<?php the_permalink(); ?>"><i class="fas fa-envelope"></i></a>
              </div>
            </header>

            <div class="single__content" itemprop="description"> 
              <?php the_content(); ?>
            </div>

          </article>

      </div>
    </div>
  </section>


  <?php get_template_part('parts/cta'); ?>

</main>

<?php get_template_part('parts/footer'); ?>
